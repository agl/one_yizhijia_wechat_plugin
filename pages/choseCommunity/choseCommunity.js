Page({
  data:{
    token:wx.getStorageSync('token')
  },
  onLoad:function(){
    var self = this;
    self.setData({
      token: wx.getStorageSync('token')
    })
      wx.request({
        url: getApp().webCase +'client/info/member-community-list',
      data: { 
        token: self.data.token
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code==200){
          self.setData({
            communityInfoList: res.data.data.communityInfoList
          })
        } else {
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function(){
            wx.navigateTo({
              url: '../login/login',
            })
          },2000)
        } 
      }
    })
  },
  goBack:function(e){
    var self =this;
    wx.request({
      url: getApp().webCase +'smartdoor/samll-program/client/save-member-community',
      data: {
        token: self.data.token,
        cmnt_c: e.currentTarget.dataset.communityfid
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
      }
    })
    var pagelist = getCurrentPages();
    if (pagelist.length > 1) {
      //获取上一个页面实例对象
      var prePage = pagelist[pagelist.length - 2];
      prePage.getBackData(e.currentTarget.dataset.name, e.currentTarget.dataset.communityfid);

      wx.navigateBack({
        delta: 1
      })
    }
  },
  onShareAppMessage: function () {

  },
})