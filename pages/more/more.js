Page({
  data: {
    
  },
  onLoad: function (options) {
    var self = this;
    self.setData({
      token:wx.getStorageSync('token')
    })
    wx.request({
      url: getApp().webCase +'smartdoor/info/cmnt-device-list',
      data: {
        token: this.data.token,
        cmnt_c: options.cmnt_c,
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code==200){
           self.setData({
          list:res.data.data
        })
        } else {
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }
      }
  })
  },
  KJ: function (e) {
    wx.request({
      url: getApp().webCase +'smartdoor/samll-program/client/open',
      data: {
        token: this.data.token,
        directory: e.currentTarget.dataset.dir,
        cmnt_c: e.currentTarget.dataset.cmnt
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code==200){
          wx.showToast({
          title: res.data.other.message,
          mask: true,
          image: '../../img/ic_cuowu.png',
          duration: 2000
        })
        } else {
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }
      }
    })
  },
  open: function (e) {
    var self = this;
    wx.request({
      url: getApp().webCase +'smartdoor/client/opendoor',
      data: {
        token: self.data.token,
        dir: e.currentTarget.dataset.dir,
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.other.code == 200) {
          wx.showToast({
            title: res.data.other.message,
            icon: 'success',
            mask: true,
            duration: 2000
          })
        } else {
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
        }
      }
    })
  },
  onShareAppMessage: function () {

  }
})
