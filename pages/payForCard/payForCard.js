Page({
  data: {
    token: wx.getStorageSync('token'),
  },
  onLoad: function (options) {
    var self = this;
    self.setData({
      token: wx.getStorageSync('token'),
      parkCardFid: options.parkCardFid
    })
    wx.request({
      url: getApp().webCase +'park/card/to-client/card-recharge',
      data: {
        token: self.data.token,
        parkCardFid: self.data.parkCardFid
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.other.code != 200) {
          wx.navigateTo({
            url: '../login/login',
          })
        } else {
          self.setData({
            carNo: res.data.data.carNo,
            customerName: res.data.data.customerName,
            monthCardRuleList: res.data.data.monthCardRuleList,
            parkCardFid: res.data.data.parkCardFid,
            parkPlaceFid: res.data.data.parkPlaceFid,
            parkPlaceName: res.data.data.parkPlaceName,
            phoneNo: res.data.data.phoneNo,
            startDate: res.data.data.monthCardRuleList[0].startDate,
            endDate: res.data.data.monthCardRuleList[0].endDate,
            money: res.data.data.monthCardRuleList[0].money,
            name: res.data.data.monthCardRuleList[0].name,
            monthCount: res.data.data.monthCardRuleList[0].monthCount
          })
        }
      }
    })
  },
  chose:function(e){
    this.setData({
      startDate: this.data.monthCardRuleList[e.detail.value].startDate,
      endDate: this.data.monthCardRuleList[e.detail.value].endDate,
      money: this.data.monthCardRuleList[e.detail.value].money,
      name: this.data.monthCardRuleList[e.detail.value].name,
      monthCount: this.data.monthCardRuleList[e.detail.value].monthCount
    })
  },
  pay: function () {    
  var self = this;
    wx.login({
      success: function (res) {
        wx.request({
          url: getApp().webCase +'park/card/from-client/pay-bill',
          data: {
            token: self.data.token,
            parkCardFid: self.data.parkCardFid,
            payMethod: 206,
            code: res.code,
            monthName: self.data.name,
            monthCount: self.data.monthCount
          },
          method: 'POST',
          header: { 'Content-Type': 'application/x-www-form-urlencoded' },
          success: function (res) {
            if (res.data.other.code == 200) {
              wx.requestPayment(
                {
                  'timeStamp': res.data.data.timestamp,
                  'nonceStr': res.data.data.nonceStr,
                  'package': res.data.data.packageStr,
                  'signType': res.data.data.signType,
                  'paySign': res.data.data.paySign,
                  'success': function (res) {
                    wx.navigateBack();
                  },
                  'fail': function (res) { },
                  'complete': function (res) { }
                })
            } else {
              wx.showToast({
                title: res.data.other.message,
                mask: true,
                image: '../../img/ic_cuowu.png',
                duration: 2000
              })
              setTimeout(function () {
                wx.navigateTo({
                  url: '../login/login',
                })
              }, 2000)
            }
          }
        })
      }
    })
  },
  delate: function () {
    var self = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          wx.request({
            url: getApp().webCase +'park/card/from-client/card-delete',
            data: {
              token: self.data.token,
              parkCardFids: self.data.parkCardFid
            },
            method: 'POST',
            header: { 'Content-Type': 'application/x-www-form-urlencoded' },
            success: function (res) {
              if (res.data.other.code == 200) {
                wx.showToast({
                  title: res.data.other.message,
                  mask: true,
                  icon: 'success',
                  duration: 2000
                })
                setTimeout(function () {
                  wx.navigateTo({
                    url: '../monthCard/monthCard',
                  })
                }, 2000)
              }
              if (res.data.other.code == 123) {
                wx.showToast({
                  title: res.data.other.message,
                  mask: true,
                  image: '../../img/ic_cuowu.png',
                  duration: 2000
                })
                setTimeout(function () {
                  wx.navigateTo({
                    url: '../login/login',
                  })
                }, 2000)
              }
            }
          })
        } else if (sm.cancel) {
        }
      }
    })
  },
  onShareAppMessage: function () {
  },

})