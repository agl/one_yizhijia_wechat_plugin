Page({
  data: {
    token: wx.getStorageSync('token'),
  },
  onLoad:function(){
    var self = this;
    self.setData({
      token: wx.getStorageSync('token')
    })
    wx.request({
      url: getApp().webCase +'park/card/to-client/card-list',
      data: {
        token: self.data.token,
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code!=200){
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }else{
          self.setData({
            list: res.data.data.cardList,
          })
        }
      }
    })
  },
  detail:function(e){
    if (e.currentTarget.dataset.type=='免费卡'){
      if (e.currentTarget.dataset.approvestate==0){
        wx.showModal({
          title: '温馨提示',
          content: '您的车卡申请正在审核,请稍后',
          success: function (res) {
          }
        })
      } else if (e.currentTarget.dataset.approvestate == 1){
        wx.navigateTo({
          url: '../freeCard/freeCard?parkCardFid=' + e.currentTarget.dataset.fid,
        })
      } else if (e.currentTarget.dataset.approvestate == 2){
        wx.showModal({
          title: '温馨提示',
          content: '您的车卡申请已被拒绝,请填写完善真实的信息!',
          success: function (res) {
          }
        })
      }
    } else if (e.currentTarget.dataset.type=='月租卡'){
      if (e.currentTarget.dataset.approvestate == 0) {
        wx.showModal({
          title: '温馨提示',
          content: '您的车卡申请正在审核,请稍后',
          success: function (res) {
          }
        })
      } else if (e.currentTarget.dataset.approvestate == 1) {
        wx.navigateTo({
          url: '../payForCard/payForCard?parkCardFid=' + e.currentTarget.dataset.fid,
        })
      } else if (e.currentTarget.dataset.approvestate == 2) {
        wx.showModal({
          title: '温馨提示',
          content: '您的车卡申请已被拒绝,请填写完善真实的信息!',
          success: function (res) {
          }
        })
      }
    }
  }, 
  onPullDownRefresh: function (){
    this.onLoad();
    wx.stopPullDownRefresh();
  },
  onShareAppMessage: function () {

  }
})