Page({
  data: {

  },
  onLoad:function(options){
    this.setData({
      parkPlaceName: options.parkPlaceName,
      carNo: options.carNo,
      outTime: options.outTime,
      costMoney: options.costMoney
    })
  },
  goBack:function(){
    wx.reLaunch({
      url: '../index/index',
    })
  }
})