Page({
  data:{
    class2:'active',
    display1:'none',
    display2:'block'
  },
  textMagnify1: function () {
    this.setData({
      size1: '20rpx',
      top1: '10rpx'
    })
  },
  textNarrow1: function (e) {
    if(e.detail.value==""){
      this.setData({
        size1: '40rpx',
        top1: '30rpx'
      })
    }else{
      this.setData({
        size1: '20rpx',
        top1: '10rpx'
      })
    }
  },
  textMagnify2: function () {
    this.setData({
      size2: '20rpx',
      top2: '10rpx'
    })
  },
  textNarrow2: function (e) {
    if (e.detail.value == "") {
      this.setData({
        size2: '40rpx',
        top2: '30rpx'
      })
    } else {
      this.setData({
        size2: '20rpx',
        top2: '10rpx'
      })
    }
  },
  textMagnify3: function () {
    this.setData({
      size3: '20rpx',
      top3: '10rpx'
    })
  },
  textNarrow3: function (e) {
    if (e.detail.value == "") {
      this.setData({
        size3: '40rpx',
        top3: '30rpx'
      })
    } else {
      this.setData({
        size3: '20rpx',
        top3: '10rpx'
      })
    }
  },
  textMagnify4: function () {
    this.setData({
      size4: '20rpx',
      top4: '10rpx'
    })
  },
  textNarrow4: function (e) {
    if (e.detail.value == "") {
      this.setData({
        size4: '40rpx',
        top4: '30rpx'
      })
    } else {
      this.setData({
        size4: '20rpx',
        top4: '10rpx'
      })
    }
  },
  textMagnify5: function () {
    this.setData({
      size5: '20rpx',
      top5: '10rpx'
    })
  },
  textNarrow5: function (e) {
    if (e.detail.value == "") {
      this.setData({
        size5: '40rpx',
        top5: '30rpx'
      })
    } else {
      this.setData({
        size5: '20rpx',
        top5: '10rpx'
      })
    }
  },
  textMagnify6: function () {
    this.setData({
      size6: '20rpx',
      top6: '10rpx'
    })
  },
  textNarrow6: function (e) {
    if (e.detail.value == "") {
      this.setData({
        size6: '40rpx',
        top6: '30rpx'
      })
    } else {
      this.setData({
        size6: '20rpx',
        top6: '10rpx'
      })
    }
  },
  inputAccount:function(e){
    this.setData({
      userName: e.detail.value
    })
  },
  inputPassword:function(e){
    this.setData({
      userPwd: e.detail.value
    })
  },
  loginBtnClick: function (e) {
    wx.request({
      url: getApp().root+'memberSYS-m/client/login.do',
      data: {
        sc: 'AglhzYsq',
        user: this.data.userName,
        pwd: this.data.userPwd
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code==200){
          wx.showToast({
            title: res.data.other.message,
            icon: 'succes',
            duration: 2000,
            mask: true
          })
          wx.setStorageSync('token', res.data.data.memberInfo.token);
          setTimeout(function () {
            wx.reLaunch({    
              url: '../index/index'

            })
          }, 2000)
        }else{
          wx.showToast({
            title: res.data.other.message,
            image: '../../img/ic_cuowu.png',
            duration: 2000,
            mask: true
          })
        }
      }
    })
  },
  change1:function(){
    this.setData({
      class1 : 'active',
      class2 : '',
      display1:'block',
      display2:'none',
    })
  },
  change2: function () {
    this.setData({
      class1: '',
      class2: 'active',
      display1: 'none',
      display2: 'block',
    })
  },
  inputAccount1: function (e) {
    this.setData({
      userName1: e.detail.value
    })
  },
  inputCode: function (e) {
    this.setData({
      code: e.detail.value
    })
  },
  pwd1: function (e) {
    this.setData({
      pwd1: e.detail.value
    })
  },
  pwd2: function (e) {
    this.setData({
      pwd2: e.detail.value
    })
  },
  getCode:function(){
    wx.request({
      url: getApp().root +'memberSYS-m/client/validCode.do',
      data: {
        sc: 'AglhzYsq',
        phone: this.data.userName1,
        type: 'v_regPhone'
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code==200){
          wx.showToast({
            title: res.data.other.message,
            icon: 'succes',
            duration: 2000,
            mask: true
          })
        }else{
          wx.showToast({
            title: res.data.other.message,
            image: '../../img/ic_cuowu.png',
            duration: 2000,
            mask: true
          })
        }
      }
    }) 
  },
  register:function(){
    wx.request({
      url: getApp().root +'memberSYS-m/client/register.do',
      data: {
        sc: 'AglhzYsq',
        account: this.data.userName1,
        verifyCode: this.data.code,
        password1: this.data.pwd1,
        password2: this.data.pwd2
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code==200){
          wx.showToast({
            title: res.data.other.message,
            icon: 'succes',
            duration: 2000,
            mask: true
          })
        }else{
          wx.showToast({
            title: res.data.other.message,
            image: '../../img/ic_cuowu.png',
            duration: 2000,
            mask: true
          })
        }
      }
    })  
  },
  onShareAppMessage: function () {

  }
})