Page({
  data: {
    his: wx.getStorageSync('his'),
    token: wx.getStorageSync('token')
  },
  choseCity:function(){
    wx.navigateTo({
      url: '../choseCity/choseCity',
    })
  },
  getBackData: function (city) {
    var self =this;
    if (city == undefined || city == null ||  city == '') {
      city = '选择城市'
    }
    this.setData({
      city: city
    })
    wx.request({
      url: getApp().webCase +'park/place/to-client/search-park-list',
      data: {
        regionKeywords: city
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
           self.setData({
          parkPlaceList: res.data.data.parkPlaceList
        })
      }
    })
  },
  onLoad: function (options) {
    var that = this;
    that.setData({
      token: wx.getStorageSync('token')
    })
    that.getBackData();
    wx.request({
      url: getApp().webCase +'smartdoor/samll-program/client/citys',
      data: {
        token: that.data.token,
      },
      method: 'GET',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (e) {
        that.setData({
          city:e.data.data
        })
        wx.request({
          url: getApp().webCase +'park/place/to-client/search-park-list',
          data: {
            regionKeywords: e.data.data
          },
          method: 'POST',
          header: { 'Content-Type': 'application/x-www-form-urlencoded' },
          success: function (res) {
            that.setData({
              parkPlaceList: res.data.data.parkPlaceList
            })
          }
        }) 
      }
    })
      that.data.his = wx.getStorageSync('his');
      if (that.data.his == undefined || that.data.his == null || that.data.his == '') {
        that.data.his = '';
      }
      if (that.data.his != '') {
        var arr1 = that.data.his.split('^');
        if (arr1[0] != undefined || arr1[0] != null || arr1[0] != '') {
          var arr2 = arr1[0].split(',');
          that.setData({
            arr2: arr2
          })
        } else {
          that.setData({
            arr2: null
          })
        }
        if (arr1[1] != undefined || arr1[1] != null || arr1[1] != '') {
          var arr3 = arr1[1].split(',');
          that.setData({
            arr3: arr3
          })
        }else {
          that.setData({
            arr3: null
          })
        }
        if (arr1[2] != undefined || arr1[2] != null || arr1[2] != '') {
          var arr4 = arr1[2].split(',');
          that.setData({
            arr4: arr4
          })
        }else{
          that.setData({
            arr4: null
          })
        }
      } 
  },
  setFid:function(e){
    var obj = e.currentTarget.dataset.fid +','+ e.currentTarget.dataset.address+',' + e.currentTarget.dataset.name;
    var arr;
    if (this.data.his == undefined || this.data.his == null || this.data.his == '') {
      this.data.his = '';
    }
    if (this.data.his == '') {
      arr = []
    } else {
      arr = this.data.his.split('^');
    }
    if (arr.length < 3) {
      for (var i = 0; i < arr.length; i++) {
        if (obj == arr[i]) {
          obj = null;
        }
      }
      if(obj!=null){
        arr.push(obj);
      }
    } else {
      for (var i = 0; i < arr.length; i++) {
        if(obj == arr[i]){
          obj = null; 
        }
      }
      if(obj!=null){
        arr.shift();
        arr.push(obj);
      }
    }
    this.data.his = arr.join('^');
    wx.setStorageSync('his', this.data.his);
    wx.navigateTo({
      url: '../keyboard/keyboard?parkPlaceName=' + e.currentTarget.dataset.name + '&parkPlaceFid=' + e.currentTarget.dataset.fid,
    })
  },
  clear:function(e){
    var self = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          wx.removeStorageSync('his');
          self.setData({
            arr2: null,
            arr3: null,
            arr4: null,
            his: null
          })
        } else if (sm.cancel) {
        }
      }
    })
   
  },
  onShareAppMessage: function () {

  },
  wxSearchInput: function (e) {
    var self = this;
    wx.request({
      url: getApp().webCase +'park/place/to-client/search-park-list',
      data: {
        regionKeywords: self.data.city,
        naKeywords: e.detail.value
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
           self.setData({
          parkPlaceList: res.data.data.parkPlaceList
        })
      }
    }) 
  },
  wxSearch:function(){

  },
})
