//index.js
//获取应用实例


Page({
   data:{
    // text:"这是一个页面"
    token: wx.getStorageSync('token'),
    actionSheetHidden:true,
    actionSheetItems:[
    ],
    menu:''
  },
   getBackData: function (communityName, communityfid) {
     if (communityName == undefined || communityName == null || communityName == '') {
       this.setData({
         communityName: '选择社区'
       })
     } else {
       this.setData({
         communityName: communityName,
         communityfid: communityfid
       })
     }
     var self = this;
     wx.request({
       url: getApp().webCase+'smartdoor/samll-program/client/open-door-list',
       data: {
         token: self.data.token,
         cmnt_c: self.data.communityfid,
         pageSize: 3,
         page: 1
       },
       method: 'GET',
       header: { 'Content-Type': 'application/x-www-form-urlencoded' },
       success: function (res) {
         if(res.data.other.code==123){
           self.setData({
             openDoorList: null
           })
         }else{
           self.setData({
             openDoorList: res.data.data.dataList
           })
         }
       }
     })
   },
  onShow:function(){
    var self = this;
    self.setData({
      token: wx.getStorageSync('token')
    })
    this.getBackData();
    wx.request({
      url: getApp().webCase+'smartdoor/samll-program/client/communitys',
      data: {
        token: self.data.token,
      },
      method: 'GET',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        self.setData({
          communityName: res.data.data.communityName,
          communityfid: res.data.data.communityFid
        })
        wx.request({
          url: getApp().webCase +'smartdoor/samll-program/client/open-door-list',
          data: {
            token: self.data.token,
            cmnt_c: self.data.communityfid,
            pageSize: 3,
            page: 1
          },
          method: 'GET',
          header: { 'Content-Type': 'application/x-www-form-urlencoded' },
          success: function (res) {
            self.setData({
              openDoorList: res.data.data.dataList
            })
          }
        })
      }
    })
  },
  choseCommunity:function(){
    var self = this;
    wx.request({
      url: getApp().webCase + '/other/client/checkLogin',
      data: {
        token: self.data.token,
      },
      method: 'GET',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
         if(res.data.other.code==200){
           wx.navigateTo({
             url: '../choseCommunity/choseCommunity',
           })
         }else{
           wx.showToast({
             title: "您还没有登录",
             mask: true,
             image: '../../img/ic_cuowu.png',
             duration: 2000
           })
           setTimeout(function () {
             wx.navigateTo({
               url: '../login/login',
             })
           }, 2000)
         }
      }
    })
  },
  property:function(){
    var self = this;
    wx.request({
      url: getApp().webCase + '/property/contact-property/to-client/contact-info',
      data: {
        token: self.data.token,
        cmnt_c: self.data.communityfid
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        console.log(res)
        if(res.data.other.code==200){
          wx.showActionSheet({
            itemList: [res.data.data.mobileNo, res.data.data.telephoneNo],
            success: function (e) {
              if (e.tapIndex === 0) {
                wx.makePhoneCall({
                  phoneNumber: res.data.data.mobileNo,
                })
              } else if (e.tapIndex === 1){
                wx.makePhoneCall({
                  phoneNumber: res.data.data.telephoneNo,
                })
              }
            }
          })
        } else {
          wx.showToast({
            title: "您还没有登录",
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }
      }
    })
  },
  downlow:function(){
    wx.getSystemInfo({
      success: function (res) {
        console.log(res.system)
        if (res.system.indexOf('iOS')!=-1){
          wx.navigateTo({
            url: 'https://itunes.apple.com/us/app/亿社区/id1198596126?l=zh&ls=1&mt=8',
          })
        }
      }
    })
  },
  actionSheetTap:function(){
    var self = this;
    wx.request({
      url: getApp().webCase + '/other/client/checkLogin',
      data: {
        token: self.data.token,
      },
      method: 'GET',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.other.code == 200) {
          self.setData({
            actionSheetHidden: !self.data.actionSheetHidden
          })
          wx.request({
            url: getApp().webCase + 'smartdoor/info/cmnt-device-list',
            data: {
              token: self.data.token,
              cmnt_c: self.data.communityfid,
            },
            method: 'POST',
            header: { 'Content-Type': 'application/x-www-form-urlencoded' },
            success: function (e) {
              if (e.data.other.code == 200) {
                self.setData({
                  actionSheetHidden: false
                })
                if (e.data.data.length > 3) {
                  e.data.data = e.data.data.slice(0, 3)
                }
                self.setData({
                  actionSheetItems: e.data.data
                })
              }else{
                wx.showToast({
                  title: e.data.other.message,
                  mask: true,
                  image: '../../img/ic_cuowu.png',
                  duration: 2000
                })
                setTimeout(function () {
                  wx.navigateTo({
                    url: '../login/login',
                  })
                }, 2000)
              }
            }
          })
        }else {
          wx.showToast({
            title: "您还没有登录",
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        } 
      }
    })  
  },
  actionSheetbindchange:function(){
    var self = this;
    wx.request({
      url: getApp().webCase +'smartdoor/samll-program/client/open-door-list',
      data: {
        token: self.data.token,
        cmnt_c: self.data.communityfid,
        pageSize:3,
        page:1
      },
      method: 'GET',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        self.setData({
          openDoorList:res.data.data.dataList
        })
      }
    })
    self.setData({
      actionSheetHidden: !self.data.actionSheetHidden
    })
  },
  openDoor:function(e){
    var self = this;
    wx.request({
      url: getApp().webCase +'smartdoor/client/opendoor',
      data: {
        token: this.data.token,
        dir: e.currentTarget.dataset.dir,
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code==200){
          wx.showToast({
            title: res.data.other.message,
            icon: 'success',
            mask: true,
            duration: 2000
          })
          self.setData({
            actionSheetHidden: true
          })
        } else {
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }
      }
    })
  },
  KJ: function (e) {
    wx.request({
      url: getApp().webCase +'smartdoor/samll-program/client/open',
      data: {
        token: this.data.token,
        directory: e.currentTarget.dataset.dir,
        cmnt_c: e.currentTarget.dataset.cmnt_c
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code!=200){
           wx.showToast({
          title: res.data.other.message,
          mask: true,
          image: '../../img/ic_cuowu.png',
          duration: 2000
        })
        } else{
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }
      }
    })
  },
  change:function(){
    var self = this;
    wx.showModal({
      title: '提示',
      content: '是否注销登录，切换其他账号?',
      success: function (sm) {
        if (sm.confirm) {
        wx.removeStorageSync('token');
        self.setData({
          token:null
        })
        wx.navigateTo({
          url: '../login/login',
        })
        } else if (sm.cancel) {
        }
      }
    })
    
  },
  KJopen:function(e){
    wx.request({
      url: getApp().webCase +'smartdoor/client/opendoor',
      data: {
        token: this.data.token,
        dir: e.currentTarget.dataset.dir,
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.other.code == 200) {
          wx.showToast({
            title: res.data.other.message,
            icon: 'success',
            mask: true,
            duration: 2000
          })
        } else {
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }
      }
    })
  },
  more:function(){
    wx.navigateTo({
      url: '../more/more?cmnt_c=' + this.data.communityfid,
    })
  },
  monthCard:function(){
    var self = this;
    wx.request({
      url: getApp().webCase + '/other/client/checkLogin',
      data: {
        token: self.data.token,
      },
      method: 'GET',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.other.code == 200) {
          wx.navigateTo({
            url: '../monthCard/monthCard',
          })
        } else {
          wx.showToast({
            title: "您还没有登录",
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }
      }
    })
  },
  onShareAppMessage: function () {

  }
})
