Page({
  data: {
    token: wx.getStorageSync('token'),
  },
  onLoad:function(options){
    var self = this;
    self.setData({
      token: wx.getStorageSync('token'),
      parkCardFid: options.parkCardFid
    })
    wx.request({
      url: getApp().webCase +'park/card/to-client/owner-card-modify',
      data: {
        token: self.data.token,
        parkCardFid: self.data.parkCardFid
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if(res.data.other.code!=200){
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../login/login',
            })
          }, 2000)
        }else{
           self.setData({
          carNo: res.data.data.carNo,
          customerName: res.data.data.customerName,
          parkPlaceName: res.data.data.parkPlaceName,
          phoneNo: res.data.data.phoneNo.replace(res.data.data.phoneNo.substr(3, 4),'****')
        })
        }
      }
    })
  },
  delate:function(){
    var self = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          wx.request({
            url: getApp().webCase +'park/card/from-client/card-delete',
            data: {
              token: self.data.token,
              parkCardFids: self.data.parkCardFid
            },
            method: 'POST',
            header: { 'Content-Type': 'application/x-www-form-urlencoded' },
            success: function (res) {
              if(res.data.other.code == 200){
                wx.showToast({
                  title: res.data.other.message,
                  mask: true,
                  icon: 'success',
                  duration: 2000
                })
                setTimeout(function(){
                  wx.navigateTo({
                    url: '../monthCard/monthCard',
                  })
                },2000)
              }
              if (res.data.other.code==123){
                wx.showToast({
                  title: res.data.other.message,
                  mask: true,
                  image: '../../img/ic_cuowu.png',
                  duration: 2000
                })
                setTimeout(function () {
                  wx.navigateTo({
                    url: '../login/login',
                  })
                }, 2000)
              }
            }
          })
        } else if (sm.cancel) {
        }
      }
    })
  },
  onShareAppMessage: function () {

  }
})