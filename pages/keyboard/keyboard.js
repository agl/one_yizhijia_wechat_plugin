// keyboard.js
Page({
  /**
   * 页面的初始数据
   * keyboard1:首页键盘,显示省的简称
   * keyboard2:第二页键盘，显示数字和大写字母
   */
  data: {
    showModal: false,
    his: wx.getStorageSync('history'),
    isKeyboard: false,//是否显示键盘
    specialBtn: false,
    tapNum: false,//数字键盘是否可以点击
    parkingData:true,//是否展示剩余车位按钮
    isFocus:false,//输入框聚焦
    keyboardNumber: '1234567890',
    keyboardAlph: 'QWERTYUIOPASDFGHJKLZXCVBNM',
    keyboard1: '京津沪冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤川青藏琼宁渝',
    keyboard2: '',
    keyboard2For: ['删除'],
    keyboardValue: '',
    textArr: [],
    textValue: '',
    placeholder:'点此输入车牌',
    animationData: {},//动画事件
    warnMessage:'提示：请确保您填写车牌号的正确性，以免后续误交费给您造成不必要的麻烦。',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var self = this;
    self.setData({
      parkPlaceName: options.parkPlaceName,
      parkPlaceFid: options.parkPlaceFid
    })
    self.data.his = wx.getStorageSync('history')
    if (self.data.his == undefined || self.data.his == null || self.data.his == '') {
      self.data.his = ''
    }
    var arr;
    if (self.data.his == '') {
      arr = []
    } else {
      arr = self.data.his.split(',');
    }
    self.setData({
      arr: arr
    })
    //将keyboard1和keyboard2中的所有字符串拆分成一个一个字组成的数组
    self.data.keyboard1 = self.data.keyboard1.split('')
    self.data.keyboard2 = self.data.keyboard2.split('')
    self.setData({
      keyboardValue: self.data.keyboard1
    })
  },

  /**
   * 输入框显示键盘状态
   */
  showKeyboard:function(){
    var self =this;
      self.setData({
        isFocus: true,
        isKeyboard: true,
      })
  },
  /**
   * 输入框聚焦触发，显示键盘
   */
  bindFocus: function () {
    var self = this;
    if (self.data.isKeyboard) {
      //说明键盘是显示的，再次点击要隐藏键盘
      self.setData({
        isKeyboard: false,
        isFocus: false,
      })
    } else {
      //说明键盘是隐藏的，再次点击显示键盘
      self.setData({
        isFocus: true,
        isKeyboard: true,
      })
    }
  },
  /**
   * 键盘事件
   */
  tapKeyboard: function (e) {
    var self = this;
    //获取键盘点击的内容，并将内容赋值到textarea框中
    var tapIndex = e.target.dataset.index;
    var tapVal = e.target.dataset.val;
    if (self.data.textArr.length >= 7) {
      return false;
    }
    self.data.textArr.push(tapVal);
    self.data.textValue = self.data.textArr.join("");
    self.setData({
      textValue: self.data.textValue,
      keyboardValue: self.data.keyboard2,
      specialBtn: true,
    })
    if (self.data.textArr.length > 1) {
      //展示数字键盘
      self.setData({
        tapNum: true
      })
    }
  },
  /**
   * 特殊键盘事件（删除和完成）
   */
  tapSpecBtn: function (e) {
    var self = this;
    var btnIndex = e.target.dataset.index;
    var keyboardValue;
    var specialBtn;
    var tapNum;
    if (btnIndex == 0) {
      //说明是删除
      self.data.textArr.pop();
      if (self.data.textArr.length == 0) {
        //说明没有数据了，返回到省份选择键盘
        this.specialBtn = false;
        this.tapNum = false;
        this.keyboardValue = self.data.keyboard1;
      } else if (self.data.textArr.length == 1) {
        //只能输入字母
        this.tapNum = false;
        this.specialBtn = true;
        this.keyboardValue = self.data.keyboard2;
      } else {
        this.specialBtn = true;
        this.tapNum = true;
        this.keyboardValue = self.data.keyboard2;
      }
      self.data.textValue = self.data.textArr.join("");
      self.setData({
        textValue: self.data.textValue,
        keyboardValue: this.keyboardValue,
        specialBtn: this.specialBtn,
        tapNum: this.tapNum,
      })
    } else if (btnIndex == 1) {
      //说明是完成事件
      if (self.data.textArr.length < 7) {
        wx.showToast({
          title: '请输入正确的车牌号',
          mask: true,
          image: '../../img/ic_cuowu.png',
          duration: 2000
        })
      } else {
        if (self.data.his == undefined || self.data.his == null || self.data.his == '') {
          self.data.his = ''
        }
        var arr;
        if (self.data.his == '') {
          arr = []
        } else {
          arr = self.data.his.split(',');
        }
          if (arr.length < 5) {
            for(var i=0;i<arr.length;i++){
              if (self.data.textValue==arr[i]){
                self.data.textValue=null;
              }
            }
            if (self.data.textValue!=null){
              arr.unshift(self.data.textValue);
            }
          } else {
            for (var i = 0; i < arr.length; i++) {
              if (self.data.textValue == arr[i]) {
                self.data.textValue = null;
              }
            }
            if (self.data.textValue != null) {
            arr.pop();
            arr.unshift(self.data.textValue);
            }
          }
          self.setData({
            arr:arr
          })
          self.data.his = arr.join(',');
          wx.setStorageSync('history', self.data.his);
          self.setData({
            isKeyboard: false,
          })
          wx.request({
            url: getApp().webCase +'park/temporary/to-client/pay-bill',
            data: {
              parkPlaceFid: self.data.parkPlaceFid,
              carNo: self.data.textValue
            },
            method: 'POST',
            header: { 'Content-Type': 'application/x-www-form-urlencoded' },
            success: function (res) {
              if (res.data.other.code != 200) {
                wx.showToast({
                  title: res.data.other.message,
                  mask: true,
                  image: '../../img/ic_cuowu.png',
                  duration: 2000
                })
              }else {
                self.setData({
                  showModal: true,
                  parkPlaceName: res.data.data.parkPlaceName,
                  carNo: res.data.data.carNo,
                  inTime: res.data.data.inTime,
                  outTime: res.data.data.outTime,
                  totalCostTime: res.data.data.totalCostTime,
                  amount: res.data.data.amount + '元',
                  paidMoney: res.data.data.paidMoney + '元',
                  costMoney: res.data.data.costMoney + '元'
                })
              }
            }
          }) 
      }
    }
  },
  // 历史事件
  hisSearch:function(e){
    var self = this;
    self.setData({
      // showModal:true,
      isKeyboard: false,
      isFocus:true,
      textValue: e.currentTarget.dataset.text,
      textArr: e.currentTarget.dataset.text.split(''),
      specialBtn:true,
      tapNum: true,
      keyboardValue : self.data.keyboard2
    })
       
    wx.request({
      url: getApp().webCase +'park/temporary/to-client/pay-bill',
      data: {
        parkPlaceFid: self.data.parkPlaceFid,
        carNo: e.currentTarget.dataset.text
      },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.other.code != 200){
          wx.showToast({
            title: res.data.other.message,
            mask: true,
            image: '../../img/ic_cuowu.png',
            duration: 2000
          })
        }else{
          self.setData({
            showModal: true,
            parkPlaceName: res.data.data.parkPlaceName,
            carNo: res.data.data.carNo,
            inTime: res.data.data.inTime,
            outTime: res.data.data.outTime,
            totalCostTime: res.data.data.totalCostTime,
            amount: res.data.data.amount+'元',
            paidMoney: res.data.data.paidMoney+'元',
            costMoney: res.data.data.costMoney+'元',
            parkPlaceFid: res.data.data.parkPlaceFid
          })
        }
      }
    }) 
  },
  /**
   * 点击查询剩余车位按钮
   */
  clear:function(){
    var self = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          self.setData({
            arr: null,
            his: null
          })
          wx.removeStorageSync('history')
        } else if (sm.cancel) {
        }
      }
    })
  },
  pay:function(){
    var self = this;
    wx.login({
      success: function (res) {
        wx.request({
          url: getApp().webCase +'park/temporary/from-client/pay-bill',
          data: {
            parkPlaceFid: self.data.parkPlaceFid,
            carNo: self.data.carNo,
            payMethod: 206,
            code: res.code
          },
          method: 'POST',
          header: { 'Content-Type': 'application/x-www-form-urlencoded' },
          success: function (res) {
            if(res.data.other.code==200){
               wx.requestPayment(
              {
                'timeStamp': res.data.data.timestamp,
                'nonceStr': res.data.data.nonceStr,
                'package': res.data.data.packageStr,
                'signType': res.data.data.signType,
                'paySign': res.data.data.paySign,
                'success': function (res) {
                  wx.navigateTo({
                    url: '../paySuccess/paySuccess?parkPlaceName=' + self.data.parkPlaceName + '&carNo=' + self.data.carNo + '&outTime=' + self.data.outTime + '&costMoney=' + self.data.costMoney,
                  })
                },
                'fail': function (res) { },
                'complete': function (res) { }
              })
            } else{
              wx.showToast({
                title: res.data.other.message,
                mask: true,
                image: '../../img/ic_cuowu.png',
                duration: 2000
              })
            }
          }
        })
      }
    }) 
  },
  onReachBottom: function () {
    wx.stopPullDownRefresh()
  },
  close:function(){
    this.hideModal();
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /**
   * 弹出框蒙层截断touchmove事件
   */
  preventTouchMove: function () {
  },
  /**
   * 隐藏模态对话框
   */
  hideModal: function () {
    this.setData({
      showModal: false
    });
  }
})